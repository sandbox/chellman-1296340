(function ($) {

/**
 * Add expander and collapsers for text-expando fields
 */
Drupal.behaviors.expandoCollapso = {
  attach: function (context, settings) {
		$collapsos = $('.text-exp-col', context);

		$collapsos.find('.text-exp-col-teaser :last').append(' <a class="expando" href="#">[...]</a>');
		$collapsos.find('.text-exp-col-full :last').append(' <a class="collapso" href="#">[...]</a>');
		
		$collapsos.click(function(e) {
			var $t = $(e.target);
			var $post = $(this);
			
			if ($t.is('.expando')) {
				$t.parents('.text-exp-col-teaser').addClass('element-hidden').siblings('.text-exp-col-full').removeClass('element-hidden');
				e.preventDefault();
				
			} else if ($t.is('.collapso')) {
				$t.parents('.text-exp-col-full').addClass('element-hidden').siblings('.text-exp-col-teaser').removeClass('element-hidden');
				e.preventDefault();
				
			}
		});
  }
};

})(jQuery);
